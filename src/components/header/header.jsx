import React from "react";
import styled from "styled-components";
const Header = () => {
  const Heading = styled.div`
    color: white;
    font-size: 20px;
    background-color: #3b3b61;
    padding: 20px 0px 20px 15px;
    display: grid;
    grid-template-columns: 1fr;
    grid-template-rows: 1fr;
  `;
  return <Heading>Weather Forecast (5 days)</Heading>;
};

export default Header;
