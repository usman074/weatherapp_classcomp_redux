import React from "react";
import { Select } from "./style";

const DropdownList = ({ name, options, value, onFilterChange }) => {
  return (
    <div>
      <Select
        onChange={(e) => onFilterChange(e)}
        value={value}
        name={name}
        id={name}
      >
        {options.map((option) => (
          <option key={option.id} value={option.id}>
            {option.name}
          </option>
        ))}
      </Select>
    </div>
  );
};

export default DropdownList;
