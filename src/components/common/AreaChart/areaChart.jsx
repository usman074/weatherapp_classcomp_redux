import React from "react";
import { Chart } from "react-charts";

const AreaChart = ({ weatherData, date }) => {
  const renderedData = weatherData[date[0]];

  const data = [
    {
      label: "Temp Chart",
      data: renderedData.map((weather) => {
        return [weather.dt_txt.split(" ")[1], weather.main.temp_max];
      }),
    },
  ];

  const series = () => ({
    type: "area",
  });

  const axes = React.useMemo(
    () => [
      { primary: true, type: "ordinal", position: "bottom" },
      { type: "linear", position: "left" },
    ],
    []
  );
  return (
    <div
      style={{
        width: "400px",
        height: "300px",
      }}
    >
      <Chart data={data} series={series} axes={axes} />
    </div>
  );
};

export default AreaChart;
