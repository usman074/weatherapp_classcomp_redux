import styled from "styled-components";

export const CustomInput = styled.input`
  width: 250px;
`;
export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
`;
export const CustomIcon = styled.i`
    border: 1px solid black;
    padding 5px 15px 5px 15px;
    cursor: pointer
  `;
