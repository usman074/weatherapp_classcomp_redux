import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchWeather } from "../../actions/weatherAction";
import SearchBar from "../common/SearchBar";
import DropdownList from "../common/DropdownList";
import { ApiKey } from "../../config/config.json";
import { Wrapper, FilterWrapper } from "./style";
class Filter extends Component {
  state = {
    filterSelected: "",
    dropDownItems: [],
    searchQuery: "",
  };

  componentDidMount() {
    const dropDownItems = [
      { id: "", name: "Search By" },
      { id: "q", name: "City Name" },
      { id: "id", name: "Zip Code" },
    ];
    this.setState({ dropDownItems });
  }

  handleChange = ({ currentTarget: input }) => {
    const { value, name } = input;
    let data = { ...this.state };
    data[name] = value;
    this.setState(data);
  };

  submit = () => {
    const { filterSelected, searchQuery } = this.state;
    if (!filterSelected || !searchQuery) {
      alert("Invalid Values");
      return;
    }
    this.props.fetchWeather({
      searchQuery,
      filterSelected,
      ApiKey,
    });
  };

  render() {
    const { filterSelected, dropDownItems, searchQuery } = this.state;

    return (
      <Wrapper>
        <FilterWrapper>
          <DropdownList
            name="filterSelected"
            value={filterSelected}
            options={dropDownItems}
            onFilterChange={this.handleChange}
          />
          <SearchBar
            name="searchQuery"
            value={searchQuery}
            onChange={this.handleChange}
            onClick={this.submit}
          />
        </FilterWrapper>
      </Wrapper>
    );
  }
}

export default connect(null, { fetchWeather })(Filter);
