import React, { Component } from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import AreaChart from "../common/AreaChart";

import {
  Wrapper,
  CityWrapper,
  CityDetail,
  OtherTemperatureFactors,
  TemperatureDetail,
  Scale,
  ForecastPanelWrapper,
  SelectedDayStyles,
  AverageTemp,
  ResponsiveImage,
  AreaChartWrapper,
} from "./style";
import {
  getDays,
  groupDataByDate,
  getAverageWeather,
  getIcon,
  getSelectionStyle,
  weatherDetail,
} from "./utils";

class WeatherDetails extends Component {
  state = {
    days: [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday",
    ],
    groupedWeatherByDate: [],
    selectedDay: 0,
    scale: true,
  };

  componentWillReceiveProps(nextProps) {
    const { error, weatherList } = nextProps;
    if (error) {
      alert(error);
      return;
    }
    const groupedWeatherByDate = groupDataByDate(weatherList.list);
    this.setState({ groupedWeatherByDate });
  }

  handleSelectedDay = (index) => {
    const selectedDay = index;
    this.setState({ selectedDay });
  };

  handleTempScale = () => {
    const { scale } = this.state;
    const scaleChanged = !scale;
    this.setState({ scale: scaleChanged });
  };

  weatherDataForChart = () => {};

  render() {
    const SelectedDay = styled.div`
      ${SelectedDayStyles}
    `;

    if (Object.keys(this.props.weatherList).length === 0) return null;

    const { city } = this.props.weatherList;
    const { days, groupedWeatherByDate, selectedDay, scale } = this.state;
    const { sortedDays, mappedDays } = getDays(days);

    const weather = weatherDetail(
      selectedDay,
      Object.keys(mappedDays[selectedDay]),
      groupedWeatherByDate
    );
    return (
      <Wrapper>
        <CityWrapper>
          <CityDetail>
            {city.name} <span>,{city.country}</span>
          </CityDetail>
          <div>{sortedDays[selectedDay]}</div>
          <div>{weather.weather_desc}</div>
        </CityWrapper>
        <TemperatureDetail>
          <ResponsiveImage
            src={getIcon(
              Object.keys(mappedDays[selectedDay]),
              groupedWeatherByDate
            )}
            alt="weather-icon"
          />
          <h1>
            {scale ? weather.avg_temp_centigrade : weather.avg_temp_fahrenheit}
          </h1>
          <Scale onClick={this.handleTempScale}>
            <sup>&#176; C | </sup>
            <sup>&#176; F</sup>
          </Scale>
        </TemperatureDetail>
        <OtherTemperatureFactors>
          <div>
            <p>Pressure: {weather.avg_pressure} hPa</p>
            <p>Humidity: {weather.avg_humidity}%</p>
            <p>Wind Speed: {weather.avg_wind} m/s</p>
          </div>
        </OtherTemperatureFactors>

        <ForecastPanelWrapper>
          {sortedDays.map((day, index) => (
            <SelectedDay
              selected={getSelectionStyle(selectedDay, index)}
              key={index}
              onClick={() => this.handleSelectedDay(index)}
            >
              <p>{day}</p>
              <img
                src={getIcon(
                  Object.keys(mappedDays[index]),
                  groupedWeatherByDate
                )}
                alt="weather-icon"
              />
              <AverageTemp>
                <p>
                  <strong>
                    {getAverageWeather(
                      Object.keys(mappedDays[index]),
                      groupedWeatherByDate,
                      1,
                      "temp_max"
                    )}
                    <span>&#176;</span>
                  </strong>
                </p>

                <p>
                  {getAverageWeather(
                    Object.keys(mappedDays[index]),
                    groupedWeatherByDate,
                    1,
                    "temp_min"
                  )}
                  <span>&#176;</span>
                </p>
              </AverageTemp>
            </SelectedDay>
          ))}
        </ForecastPanelWrapper>

        <AreaChartWrapper>
          <AreaChart
            weatherData={groupedWeatherByDate}
            date={Object.keys(mappedDays[selectedDay])}
          />
        </AreaChartWrapper>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => ({
  weatherList: state.weather.weatherObj,
  error: state.weather.error,
});

export default connect(mapStateToProps)(WeatherDetails);
