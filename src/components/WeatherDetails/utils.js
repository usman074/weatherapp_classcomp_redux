export function getDays(days) {
  const day = new Date().getDay();
  let sortedDays = days.slice(day - 1, 7);
  if (sortedDays.length < 5) {
    sortedDays = [...sortedDays, ...days.slice(0, 5 - sortedDays.length)];
  } else {
    sortedDays = sortedDays.slice(0, 5);
  }
  const mappedDays = mapDateToDays(sortedDays);
  return { sortedDays, mappedDays };
}

function mapDateToDays(days) {
  let currentDate = new Date();
  currentDate.setDate(currentDate.getDate() - 1);
  return days.map((day) => {
    currentDate.setDate(currentDate.getDate() + 1);
    return { [currentDate.getDate()]: day };
  });
}

export function groupDataByDate(weatherList) {
  return weatherList.reduce((accumulator, currentValue) => {
    const date = new Date(currentValue.dt_txt).getDate();
    accumulator[date] = accumulator[date] || [];
    accumulator[date].push(currentValue);
    return accumulator;
  }, {});
}

export function getAverageWeather(date, groupedWeatherByDate, round, propName) {
  const weatherArray = groupedWeatherByDate[date[0]];
  const avgTemp = calcAvgWeather(weatherArray, round, propName);
  return avgTemp;
}

function calcAvgWeather(weatherArray, round, propName) {
  const { length } = weatherArray;
  const avg_weather = weatherArray.reduce((sum, weather) => {
    return sum + weather.main[propName];
  }, 0);

  const average = avg_weather / length;
  return round ? Math.round(average) : Math.round(average * 100) / 100;
}

export function getIcon(date, groupedWeatherByDate) {
  const data = groupedWeatherByDate[date[0]];
  return `https://openweathermap.org/img/w/${data[0].weather[0].icon}.png`;
}

export function getSelectionStyle(selectedDay, index) {
  if (selectedDay === index) {
    return true;
  }
  return false;
}

export function weatherDetail(index, date, groupedWeatherByDate) {
  if (index === 0) {
    const weatherNow = todayWeather(date, groupedWeatherByDate);
    const { temp_max, humidity, pressure } = weatherNow.main;
    const avg_temp_centigrade = Math.round(temp_max);
    const avg_temp_fahrenheit = Math.round(avg_temp_centigrade * (9 / 5) + 32);
    const avg_wind = weatherNow.wind.speed;
    const weather_desc = weatherNow.weather[0].main;
    return {
      avg_wind,
      avg_temp_fahrenheit,
      avg_temp_centigrade,
      weather_desc,
      avg_humidity: humidity,
      avg_pressure: pressure,
    };
  } else {
    const avg_temp_centigrade = getAverageWeather(
      date,
      groupedWeatherByDate,
      1,
      "temp_max"
    );
    const avg_temp_fahrenheit = Math.round(avg_temp_centigrade * (9 / 5) + 32);
    const avg_humidity = getAverageWeather(
      date,
      groupedWeatherByDate,
      0,
      "humidity"
    );
    const avg_pressure = getAverageWeather(
      date,
      groupedWeatherByDate,
      0,
      "pressure"
    );
    const avg_wind = calcAvgWindSpeed(date, groupedWeatherByDate);
    const weather_desc = weatherDesc(date, groupedWeatherByDate);
    return {
      avg_temp_centigrade,
      avg_temp_fahrenheit,
      avg_humidity,
      avg_pressure,
      avg_wind,
      weather_desc,
    };
  }
}

function calcAvgWindSpeed(date, groupedWeatherByDate) {
  const weatherArray = groupedWeatherByDate[date[0]];
  const { length } = weatherArray;

  const wind = weatherArray.reduce((sum, weather) => {
    return sum + weather.wind.speed;
  }, 0);

  return Math.round((wind / length) * 100) / 100;
}

function weatherDesc(date, groupedWeatherByDate) {
  const weatherArray = groupedWeatherByDate[date[0]];

  return weatherArray[0].weather[0].main;
}

function todayWeather(date, groupedWeatherByDate) {
  const weatherArray = groupedWeatherByDate[date[0]];
  const time = new Date().getTime();

  return weatherArray.find((weather) => {
    const weatherTime = new Date(weather.dt_txt).getTime();
    return weatherTime > time;
  });
}
