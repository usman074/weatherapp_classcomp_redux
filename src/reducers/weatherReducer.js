import { FETCH_WEATHER_SUCCESS } from "../actions/weatherAction";
import { FETCH_WEATHER_FAILURE } from "../actions/weatherAction";

const initialState = {
  weatherObj: {},
  error: "",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_WEATHER_SUCCESS:
      return {
        ...state,
        weatherObj: action.payload.data,
        error: null,
      };
    case FETCH_WEATHER_FAILURE:
      return {
        ...state,
        weatherObj: {},
        error: action.payload.response.data.message,
      };
    default:
      return state;
  }
}
