import { urls } from "../utils/urls";

export const FETCH_WEATHER = "FETCH_WEATHER";
export const FETCH_WEATHER_SUCCESS = "FETCH_WEATHER_SUCCESS";
export const FETCH_WEATHER_FAILURE = "FETCH_WEATHER_FAILURE";

const { weatherUrl } = urls;

export const fetchWeather = ({ filterSelected, searchQuery, ApiKey }) => ({
  types: [FETCH_WEATHER, FETCH_WEATHER_SUCCESS, FETCH_WEATHER_FAILURE],
  url: weatherUrl,
  method: "get",
  payload: {},
  queryParams: {
    [filterSelected]: searchQuery,
    appid: ApiKey,
    units: "metric",
  },
  meta: {
    type: "api",
  },
});
