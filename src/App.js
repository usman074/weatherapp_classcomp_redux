import React from "react";
import "./App.css";
import { Provider } from "react-redux";
import store from "./store";
import Filter from "./components/Filter";
import Header from "./components/header";
import WeatherDetails from "./components/WeatherDetails";
function App() {
  return (
    <Provider store={store}>
      <Header />
      <Filter />
      <WeatherDetails />
    </Provider>
  );
}

export default App;
