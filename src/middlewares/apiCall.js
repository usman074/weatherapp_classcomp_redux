import http from "../services/httpService";

const callApi = (store) => (next) => async (action) => {
  if (typeof action === "undefined") {
    return next(action);
  }

  if (!action.meta || action.meta.type !== "api") {
    return next(action);
  }

  const { types, url, method, payload, queryParams } = action;

  let response;
  if (method === "get") {
    try {
      // response = await http.get(url, {
      //   params: queryParams,
      // });

      response = await http.axios({
        method: method,
        url: url,
        params: queryParams,
      });
      const newAction = {
        type: types[1],
        payload: response,
      };
      store.dispatch(newAction);
    } catch (error) {
      const newAction = {
        type: types[2],
        payload: error,
      };
      store.dispatch(newAction);
    }
  } else {
    try {
      response = await http.axios({
        method: method,
        url: url,
        data: payload,
      });
      const newAction = {
        type: types[1],
        payload: response,
      };
      store.dispatch(newAction);
    } catch (error) {
      const newAction = {
        type: types[2],
        payload: error,
      };
      store.dispatch(newAction);
    }
  }

  //   const { url } = action.meta;
  //   const fetchOptions = Object.assign({}, action.meta);
};

export default callApi;
